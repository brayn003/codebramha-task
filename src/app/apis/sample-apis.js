import axios from 'axios';

const URL_GET_SAMPLE_TEXT = 'http://www.randomtext.me/api/';

export function getSampleText() {
  return axios.get(URL_GET_SAMPLE_TEXT).then(res => res.data);
}

export default {
  getSampleText,
};

