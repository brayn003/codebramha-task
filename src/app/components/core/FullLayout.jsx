import React, { Fragment } from 'react';
import { Route, Switch } from 'react-router-dom';

import PracticeView from '../views/PracticeView';

export default function FullLayout() {
  return (
    <Fragment>
      <Switch>
        <Route path="/" component={PracticeView} />
      </Switch>
    </Fragment>
  );
}
