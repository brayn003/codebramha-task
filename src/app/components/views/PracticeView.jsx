import React from 'react';
import { Card } from 'antd';

import SampleTextContainer from '../samples/SampleTextContainer';
import SampleInputContainer from '../samples/SampleInputContainer';
import SampleTextTimerContainer from '../timer/SampleTextTimerContainer';

export default function PracticeView() {
  return (
    <div className="view-practice">
      <Card className="practice-card">
        <SampleTextContainer />
        <SampleInputContainer />
        <SampleTextTimerContainer />
      </Card>
    </div>
  );
}
