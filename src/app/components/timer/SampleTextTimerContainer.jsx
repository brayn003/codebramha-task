import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { notification } from 'antd';

import { setSampleStatusAction } from '../../reducers/samples/text-active';

import Timer from './Timer';

function SampleTextTimerContainer(props) {
  if (props.status !== 'running') {
    return null;
  }
  return (
    <Timer
      onStopTimer={() => {
        props.onStopTimer('failed');
        notification.error({ message: 'Failed the task', description: 'Timer ran out', duration: 0 });
      }}
    />
  );
}

SampleTextTimerContainer.propTypes = {
  status: PropTypes.string.isRequired,
  onStopTimer: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    status: state.samples.textActive.get('status'),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    onStopTimer: setSampleStatusAction,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SampleTextTimerContainer);
