import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Timer extends Component {
  static propTypes = {
    onStopTimer: PropTypes.func,
  }

  static defaultProps = {
    onStopTimer: () => {},
  }

  constructor() {
    super();
    this.state = { seconds: 180 };
  }

  componentDidMount() {
    this.startTimer();
  }

  startTimer = () => {
    const { onStopTimer } = this.props;
    this.timerInterval = setInterval(() => {
      if (this.state.seconds > 0) {
        this.setState({ seconds: (this.state.seconds - 1) });
      } else {
        clearInterval(this.timerInterval);
        onStopTimer();
      }
    }, 1000);
  }

  render() {
    const { seconds } = this.state;
    return <p className="timer">{seconds} seconds left</p>;
  }
}
