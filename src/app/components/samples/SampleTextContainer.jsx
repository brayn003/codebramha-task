import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import SampleText from './SampleText';
import { requestSampleTextActiveAction } from '../../reducers/samples/text-active';

class SampleTextContainer extends Component {
  static propTypes = {
    correctText: PropTypes.string,
    wrongText: PropTypes.string,
    normalText: PropTypes.string,
    loading: PropTypes.bool,
    error: PropTypes.string,
    getSampleText: PropTypes.func.isRequired,
  }

  static defaultProps = {
    correctText: null,
    wrongText: null,
    normalText: null,
    loading: false,
    error: null,
  }

  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    this.props.getSampleText();
  }

  render() {
    const {
      correctText, wrongText, normalText, loading, error,
    } = this.props;
    return (
      <SampleText
        correctText={correctText}
        wrongText={wrongText}
        normalText={normalText}
        loading={loading}
        error={error}
      />
    );
  }
}

function mapStateToProps(state) {
  const { textActive } = state.samples;
  return {
    normalText: textActive.getIn(['values', 'normalText']),
    wrongText: textActive.getIn(['values', 'wrongText']),
    correctText: textActive.getIn(['values', 'correctText']),
    loading: textActive.get('loading'),
    error: textActive.get('error'),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSampleText: requestSampleTextActiveAction,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SampleTextContainer);
