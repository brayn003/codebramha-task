import React from 'react';
import PropTypes from 'prop-types';

import './SampleText.less';

export default function SampleText(props) {
  const {
    normalText, wrongText, correctText, loading, error,
  } = props;
  if (loading) {
    return 'loading ...';
  }
  if (error) {
    return error;
  }
  return (
    <p>
      <span className="sample-correct-text">{correctText}</span>
      <span className="sample-wrong-text">{wrongText}</span>
      <span className="sample-normal-text">{normalText}</span>
    </p>
  );
}

SampleText.propTypes = {
  correctText: PropTypes.string,
  wrongText: PropTypes.string,
  normalText: PropTypes.string,
  loading: PropTypes.bool,
  error: PropTypes.string,
};

SampleText.defaultProps = {
  correctText: null,
  wrongText: null,
  normalText: null,
  loading: false,
  error: null,
};

