import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import SampleInput from './SampleInput';
import { setTextsAction, setSampleStatusAction } from '../../reducers/samples/text-active';

function mapStateToProps(state) {
  return {
    source: state.samples.textActive.getIn(['values', 'sourceText']),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    onOutput: setTextsAction,
    onStartInput: setSampleStatusAction.bind(this, 'running'),
    onCompleteInput: setSampleStatusAction.bind(this, 'completed'),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SampleInput);
