import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input, notification } from 'antd';
import isNull from 'lodash/isNull';

function longestCommonPrefix(src, cmp) {
  for (let i = 0; i < cmp.length; i += 1) {
    if (src[i] !== cmp[i]) {
      if (i === 0) {
        return null;
      }
      return src.substring(0, i);
    }
  }
  return src.substring(0, cmp.length);
}


export default class SampleInput extends Component {
  static propTypes = {
    source: PropTypes.string,
    onOutput: PropTypes.func,
    onStartInput: PropTypes.func,
    onCompleteInput: PropTypes.func,
  }

  static defaultProps = {
    source: null,
    onOutput: () => {},
    onStartInput: () => {},
    onCompleteInput: () => {},
  }

  constructor() {
    super();
    this.state = { value: null };
    this.savedTillIndex = 0;
    this.started = false;
  }

  onChange = (e) => {
    const { value } = e.target;
    const { source, onOutput, onStartInput } = this.props;
    if (!this.started) {
      onStartInput();
    }
    const texts = this.getTexts(source, value);
    onOutput(texts);
  }

  getTexts(source, value) {
    const src = source.substring(this.savedTillIndex);
    const commonSubstring = longestCommonPrefix(src, value);
    let correctText = '';
    let wrongText = '';
    let normalText = '';
    if (
      value[value.length - 1] === ' ' &&
      !isNull(commonSubstring) &&
      commonSubstring.length === value.length &&
      commonSubstring[commonSubstring.length - 1] === ' '
    ) {
      this.savedTillIndex += value.length;
      correctText = source.substring(0, this.savedTillIndex);
      wrongText = '';
      normalText = source.substring(this.savedTillIndex);
      this.setState({ value: '' });
    } else {
      this.setState({ value });
      if (isNull(commonSubstring)) {
        correctText = source.substring(0, this.savedTillIndex);
        wrongText = source.substring(correctText.length, correctText.length + value.length);
      } else {
        correctText = source.substring(0, this.savedTillIndex) + commonSubstring;
        wrongText = source.substring(correctText.length, correctText.length
          + (value.length - commonSubstring.length));
      }
      normalText = source.substring((correctText.length + wrongText.length));
    }
    if (correctText === source) {
      this.props.onCompleteInput();
      this.setState({ value: null });
      notification.success({ message: 'Task completed', description: 'Task completed in time', duration: 0 });
    }
    return { normalText, wrongText, correctText };
  }

  render() {
    const { value } = this.state;
    const { source } = this.props;

    return <Input value={value} onChange={this.onChange} disabled={isNull(source)} />;
  }
}
