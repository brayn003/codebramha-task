import { combineReducers } from 'redux';

import sampleTextActiveReducer from './text-active';

export default combineReducers({
  textActive: sampleTextActiveReducer,
});

