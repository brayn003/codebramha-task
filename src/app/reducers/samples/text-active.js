import { Map } from 'immutable';

const initialState = Map({
  values: Map({
    sourceText: null,
    normalText: null,
    correctText: null,
    wrongText: null,
  }),
  status: 'not-started',
  loading: false,
  error: null,
});

export const SAMPLE_TEXT_ACTIVE_REQUESTED = 'SAMPLE/ACTIVE_TEXT_REQUESTED';
export const SAMPLE_TEXT_ACTIVE_SUCCEEDED = 'SAMPLE/ACTIVE_TEXT_SUCCEEDED';
export const SAMPLE_TEXT_ACTIVE_FAILED = 'SAMPLE/ACTIVE_TEXT_FAILED';
export const SAMPLE_TEXT_ACTIVE_SET_TEXTS = 'SAMPLE/ACTIVE_TEXT_SET_TEXTS';
export const SAMPLE_TEXT_ACTIVE_SET_STATUS = 'SAMPLE/TEXT_ACTIVE_SET_STATUS';

export default function sampleTextActiveReducer(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case SAMPLE_TEXT_ACTIVE_REQUESTED:
      return initialState.set('loading', true);

    case SAMPLE_TEXT_ACTIVE_SUCCEEDED:
      return initialState
        .setIn(['values', 'sourceText'], payload.text)
        .setIn(['values', 'normalText'], payload.text);

    case SAMPLE_TEXT_ACTIVE_FAILED:
      return initialState.set('error', payload.error);

    case SAMPLE_TEXT_ACTIVE_SET_TEXTS:
      return state.set('values', state.get('values').merge(Map(payload.texts)));

    case SAMPLE_TEXT_ACTIVE_SET_STATUS:
      return state.set('status', payload.status);

    default:
      return state;
  }
}

export function requestSampleTextActiveAction() {
  return { type: SAMPLE_TEXT_ACTIVE_REQUESTED, payload: {} };
}

export function setTextsAction(texts) {
  return { type: SAMPLE_TEXT_ACTIVE_SET_TEXTS, payload: { texts } };
}

export function setSampleStatusAction(status) {
  return { type: SAMPLE_TEXT_ACTIVE_SET_STATUS, payload: { status } };
}
