import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

// import './helpers/interceptor';
import store from './helpers/store';

import FullLayout from './components/core/FullLayout';
import '../assets/fonts/sofia/sofiapro-light-webfont.ttf';
import '../assets/fonts/sofia/sofiapro-light-webfont.woff';
import '../assets/fonts/sofia/sofiapro-light-webfont.woff2';
import '../theme/style.less';

render(
  <Provider store={store}>
    <BrowserRouter>
      <FullLayout />
    </BrowserRouter>
  </Provider>,
  document.getElementById('app'),
);
