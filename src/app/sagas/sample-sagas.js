import { takeLatest, call, put } from 'redux-saga/effects';
import { SAMPLE_TEXT_ACTIVE_REQUESTED, SAMPLE_TEXT_ACTIVE_FAILED, SAMPLE_TEXT_ACTIVE_SUCCEEDED } from '../reducers/samples/text-active';
import { getSampleText } from '../apis/sample-apis';

function* getSampleTextActiveWorker() {
  try {
    const res = yield call(getSampleText);
    const text = res.text_out.replace(/<[^>]*>/g, '');
    yield put({ type: SAMPLE_TEXT_ACTIVE_SUCCEEDED, payload: { text } });
  } catch (error) {
    yield put({ type: SAMPLE_TEXT_ACTIVE_FAILED, payload: { error } });
  }
}

export function* getSampleTextActiveSaga() {
  yield takeLatest(SAMPLE_TEXT_ACTIVE_REQUESTED, getSampleTextActiveWorker);
}

export default {
  getSampleTextActiveSaga,
};

