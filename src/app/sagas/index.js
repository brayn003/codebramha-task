import _ from 'lodash';

import { getSampleTextActiveSaga } from './sample-sagas';

const sagas = {
  getSampleTextActiveSaga,
};

export function registerSagasWithMiddleware(middleware) {
  _.values(sagas).forEach((saga) => { middleware.run(saga); });
}

export default {
  registerSagasWithMiddleware,
};

