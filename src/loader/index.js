import bodymovin from 'lottie-web';
import animationData from './loader.json';

const app = document.getElementById('loader');
// app.style.height = '100%';
// app.style.width = '100%';

bodymovin.loadAnimation({
  container: app,
  renderer: 'svg',
  loop: true,
  autoplay: true,
  animationData,
});
